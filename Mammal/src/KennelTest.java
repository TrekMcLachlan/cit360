import org.junit.Test;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static org.junit.Assert.*;


public class KennelTest {

    public DogBean[] buildDogs() {
        DogBean dog1 = new DogBean("Pug", "Fred", 4, 1.5, "Brown");
        DogBean dog2 = new DogBean("Lab", "Doug", 4, 2.6, "Yellow");
        DogBean dog3 = new DogBean("Retriever", "Goldie", 4, 2.6, "Gold");
        DogBean dog4 = new DogBean("Husky", "Balto", 4, 2.9, "Grey");
        DogBean dog5 = new DogBean("English Bulldog", "Gimp", 3, 2.2, "White");

        DogBean[]dogArray = {dog1, dog2, dog3,dog4,dog5};
        return dogArray;
    }


    public DogBean[] testDogs(){
        DogBean dog1 = new DogBean("Pug", "Fred", 4, 1.5, "Brown");
        DogBean dog2 = new DogBean("Lab", "Doug", 4, 2.6, "Yellow");
        DogBean dog3 = new DogBean("Retriever", "Goldie", 4, 2.6, "Gold");
        DogBean dog4 = new DogBean("Husky", "Balto", 4, 2.9, "Grey");
        DogBean dog5 = new DogBean("English Bulldog", "Gimp", 3, 2.2, "White");

        DogBean[]testDogs = {dog1, dog2, dog3, dog4, dog5};
        return testDogs;
    }
    @Test
    public  void testArray(){

        assertArrayEquals(testDogs(), buildDogs());

        Set test = new HashSet<DogBean>();

        DogBean fido = new DogBean("Mutt","Fido", 4, 3, "Black and White");
        DogBean yeller = new DogBean("Yellow Lab", "Old Yeller", 4, 2.5, "Yellow");
        DogBean santa = new DogBean("Grey Hound", "Santa's Little Helper", 4, 3.2, "Brown");
        DogBean krypto = new DogBean("Super Dog", "Krypto", 4, 2.0, "White");
        DogBean scooby = new DogBean("Great Dane", "Scooby Doo", 4, 4.5, "Brown and Black");
        DogBean balto = new DogBean("Wolf Husky Hybrid", "Balto", 4, 3.6, "Grey");

        test.add(fido);
        test.add(yeller);
        test.add(santa);
        test.add(krypto);
        test.add(scooby);
        test.add(balto);

        assertTrue(test.contains(fido));
        assertTrue(test.contains(yeller));
        assertTrue(test.contains(santa));
        assertTrue(test.contains(krypto));
        assertTrue(test.contains(scooby));
        assertTrue(test.contains(balto));

        test.remove(balto);
        test.remove(krypto);

        assertTrue(test.contains(fido));
        assertTrue(test.contains(yeller));
        assertTrue(test.contains(santa));
        assertFalse(test.contains(krypto));
        assertTrue(test.contains(scooby));
        assertFalse(test.contains(balto));

        Map<String, DogBean> map1 = new TreeMap<>();
        DogBean jo = new DogBean("Mutt", "Jo", 4, 1.5, "Green");
        DogBean mo = new DogBean("Mutt", "Mo", 4, 5.5, "Red");
        DogBean yo = new DogBean("Mutt", "Yo", 4, 3.5, "Blue");

        map1.put(jo.getName(), jo);
        map1.put(mo.getName(), mo);
        map1.put(yo.getName(), yo);

        assertTrue(map1.containsValue(jo));
        assertTrue(map1.containsKey("Mo"));
        assertTrue(map1.containsValue(yo));

        map1.remove("Mo",mo);
        map1.remove("Yo");

        assertFalse(map1.containsValue(yo));
        assertFalse(map1.containsValue(mo));
        assertTrue(map1.containsValue(jo));










    }

}
