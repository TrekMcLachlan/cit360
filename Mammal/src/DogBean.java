
public class DogBean extends MammalBean {

    private String breed;
    private String name;

    public DogBean(String breed, String name, int legCount, double height, String color){

        super(legCount, height, color);
        this.breed = breed;
        this.name = name;

    }
    public String getBreed(){
        return breed;

    }

    public String getName(){
        return name;

    }
    public void setBreed(String breed){
        this.breed = breed;
    }
    public void setName(String name){
        this.name = name;
    }

    @Override
    public boolean equals(Object o2){
        DogBean dog1 = this;
        DogBean dog2 = (DogBean)o2;

        if(dog1.getLegCount() == dog2.getLegCount() && dog1.getHeight() == dog2.getHeight()
                && dog1.getColor().equals(dog2.getColor()) && dog1.getName().equals(dog2.getName()) && dog1.getBreed().equals(dog2.getBreed())){
            return true;
        }
        else return false;
    }

    @Override
    public String toString(){
        return "Name: " + name + " Breed: " + breed + " Legs: " + getLegCount() + " Height: " + getHeight() + " Color: " + getColor();
    }
}