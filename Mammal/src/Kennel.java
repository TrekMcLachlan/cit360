

public class Kennel {


    public DogBean[] buildDogs() {
        DogBean dog1 = new DogBean("Pug", "Fred", 4, 1.5, "Brown");
        DogBean dog2 = new DogBean("Lab", "Doug", 4, 2.6, "Yellow");
        DogBean dog3 = new DogBean("Retriever", "Goldie", 4, 2.6, "Gold");
        DogBean dog4 = new DogBean("Husky", "Balto", 4, 2.9, "Grey");
        DogBean dog5 = new DogBean("English Bulldog", "Gimp", 3, 2.2, "White");

        DogBean[]dogArray = {dog1, dog2, dog3,dog4,dog5};
        return dogArray;
    }
    public void displayDogs(DogBean[]dogArray){
        for (int i = 0; i < dogArray.length; i++){
            System.out.println(dogArray[i]);
        }
    }

    public static void main(String[]args){
        Kennel k = new Kennel();
        DogBean[]allDogs = k.buildDogs();
        k.displayDogs(allDogs);
    }
}
