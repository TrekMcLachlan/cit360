
public class MammalBean {

    private int legCount = 0;
    private double height =0;
    private String color;

    public MammalBean(){

    }
    public MammalBean(int legCount, double height, String color){
        this.legCount = legCount;
        this.height = height;
        this.color = color;
    }

    public int getLegCount(){ return legCount;}
    public double getHeight() { return height;}
    public String getColor() { return color;}

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public void setHeight(double height){
        this.height = height;
    }

    public void setColor(String color){
        this.color = color;
    }
}