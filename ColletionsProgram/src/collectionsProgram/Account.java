package collectionsProgram;


public class Account {

    protected String accountNumber;
    protected String accountType;
    protected String givenName;
    protected String familyName;


    public Account(String accountNumber, String accountType, String givenName, String familyName){
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;

    }
    public String getAccountNumber(){return accountNumber;}
    public String getAccountType(){return accountType;}
    public String getFamilyName() {return familyName;}
    public String getGivenName() {return givenName;}

    public void setAccountNumber(String accountNumber){
        this.accountNumber = accountNumber;}

    public void setAccountType(String accountType) {
        this.accountType = accountType; }

    public void setFamilyName(String familyName) {
        this.familyName = familyName; }

    public void setGivenName(String givenName) {
        this.givenName = givenName; }

        @Override
        public String toString() {
        return "Account Number: " + accountNumber + " Account Type: " + accountType
                    + " Given Name: " + givenName + " Family Name: " + familyName;
        }
}
