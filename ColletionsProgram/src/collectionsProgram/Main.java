package collectionsProgram;
import java.util.*;
import java.io.*;
public class Main {
    public static void main(String[] args) {
        Map<String, Account> tree = new TreeMap<String, Account>();
        String File = "C:/Users/Trek/Documents/BYUI/cit360/rawData.csv";
        BufferedReader br = null;
                String line;
        try {
            int c = 0;
            br = new BufferedReader(new FileReader(File));
            while ((line = br.readLine()) != null) {

                String[] csv = line.split(",");
                if(!tree.containsKey(csv[0])) {
                    Account account = new Account(csv[0], csv[1], csv[2], csv[3]);
                    tree.put(csv[0],account);
                }
            }
            for (Map.Entry<String, Account> entry : tree.entrySet()){
                String key = entry.getKey();
                Account A = entry.getValue();
                System.out.println(A);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
